terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">=3.0.0"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "ocsserver-rg" {
  name     = "ocsserver"
  location = "eastus"
}

resource "azurerm_public_ip" "pu-ocsserver" {
  name		       = "ocsAG"
  resource_group_name = azurerm_resource_group.ocsserver-rg.name
  location            = azurerm_resource_group.ocsserver-rg.location
  allocation_method   = "Static"
}

resource "azurerm_virtual_network" "vn-ocsserver" {
  name                = "ocsserver"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.ocsserver-rg.location
  resource_group_name = azurerm_resource_group.ocsserver-rg.name
}

resource "azurerm_subnet" "s-ocsserver" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.ocsserver-rg.name
  virtual_network_name = azurerm_virtual_network.vn-ocsserver.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_network_interface" "nic-ocsserver" {
  name                = "example-nic"
  location            = azurerm_resource_group.ocsserver-rg.location
  resource_group_name = azurerm_resource_group.ocsserver-rg.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.s-ocsserver.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.pu-ocsserver.id

  }
}

resource "azurerm_linux_virtual_machine" "vm-ocsserver" {
  name                = "ocsserver"
  resource_group_name = azurerm_resource_group.ocsserver-rg.name
  location            = azurerm_resource_group.ocsserver-rg.location
  size                = "Standard_B1s"
  admin_username      = "toto"
  network_interface_ids = [
    azurerm_network_interface.nic-ocsserver.id,
  ]

  admin_ssh_key {
    username   = "toto"
    public_key = file("id_rsa.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts"
    version   = "latest"
  }
}